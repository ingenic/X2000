# X2000

#### 介绍
X2000 SDK 使用百度网盘进行发布，此处记录X2000 SDK各版本下载连接
X2000 SDK 下载链接:

链接:https://pan.baidu.com/s/1PxHJhv7j_oXkFTjAVNInxA 提取码:6svw


#### SDK介绍
SDK 主要包含一下文件夹
```
├── 01_SW
├── 02_HW
├── 03_PM&DS
└── 04_Toolchain
```

* 01_SW: 包含每次发布的软件，uboot, kernel，开发平台，文档，烧录工具，工具链等内容.
* 02_HW: 包含硬件原理图
* 03_PM&DS: 芯片手册和DataSheet.
* 04_Toolchain: 工具链. 01_SW中已经包含仅支持Xburst2系列芯片的工具链. 04_Toolchain下面的工具链为7.2.0版本，支持Xburst2 和 Xburst全芯片编译. 编译xburst2系列的芯片时，必须指定编译参数 -march=xburst2

xburst2系列芯片:
* X2000


### 版本发布记录
X2000_V12 kernel-4.4.94 Linux V4.0版本发布
---
 * 参考开发板:

     RD_X2000_HALLEY5_BASEBOARD_V2.1
     RD_X2000_HALLEY5_BASEBOARD_V2.0
     RD_X2000_HALLEY5_BASEBOARD_V1.1
     RD_X2000_HALLEY5_BASEBOARD_V1.0


 * TAGS:

     ingenic-linux-kernel4.4.94-x2000_v12-v4.0-20200724



 * 版本更新功能:

     uboot:

         1.修复uboot去掉打印之后概率性无法启动问题。
         2.支持sfc nor/nand 参数和分区信息配置。(脱离烧录工具)可配置，需要参考开发手册配置。



     kernel:

         1. GMAC驱动优化, 降低双千兆以太网丢包率。提高传输速度。
         2. SSI 驱动优化,优化最后1bit时钟持续拉高问题。
         3. USB 使用descripotr优化，提高USB传输效率。
         4. gpio 支持上下拉、驱动能力配置。
         5. H264解码驱动优化。
         6. H264编码器驱动优化。
         7. USB UVC 驱动优化。
         8. USB Serial 驱动优化。
         9. GPIO 支持休眠唤醒的状态配置。
         10. halley5_v1.0 OTA 支持。
         11. Hash/AES/RSA 驱动支持。
         12. 支持蓝牙/PCM。
         13. LCD 显示驱动优化。
         14. 支持设备树外部编译使用。



     Manhattan:

         1. 添加双摄同步库（位置：development/libsynchronous-frames）和使用demo及文档。
         2. 添加双目测距demo（位置：packages/example/App/stereo_demo）。
         3. 更新sfc文档，支持 sfc_nand/sfc_nor参数配置，脱离烧录工具的参数、分区信息烧写的介绍。
         4. 更新dtb文档。

     注：文档路径docs/doc/06_product/x2000_linux-4.4


     烧录工具：

        烧录工具支持到 V2.5.10。


 * 测试说明：

     在本次版本发布之前，对软件进行了功能测试。 

 * 外网下载：

     网盘链接：

         链接:https://pan.baidu.com/s/1PxHJhv7j_oXkFTjAVNInxA 提取码:6svw


 * 编译命令：

     编译环境：ubuntu 14.04 64bit 
     初次使用Manhatton 工程需要进入工程目录执行以下命令安装编译需要的工具：

         $ source build/envsetup.sh (初始化编译环境)
         $ autoenvsetup

     整体编译SDK:

     (1) 进入工程目录，执行以下命令：

         $ source build/envsetup.sh
         $ lunch

     (2) 选择相应的开发板：

         $ make



     (3) 在out/product/“板级”/image/ 下生成烧录所需要的文件:

         system.*
         kernel
         uboot

     支持make -jN 多线程编译。


     单独编译uboot和kernel：

     (1) 编译uboot

         $ cd <Manhatton_Project>
         $ make uboot

     

     (2) 编译kernel

         $ cd <Manhatton_Project>
         $ make kernel



     (3) 编译buildroot

         $ cd <Manhatton_Project>
         $ make buildroot



 * 问题与反馈:

     如有问题与建议请联系support@ingenic.com


---
X2000_V12 kernel-4.4.94 Linux V3.0版本发布
----
* 参考开发板:
 RD_X2000_HALLEY5_BASEBOARD_V2.0
 RD_X2000_HALLEY5_BASEBOARD_V1.0


TAGS:
 ingenic-linux-kernel4.4.94-x2000_v12-v3.0-20200610


* 升级内容:

1. buildroot 升级到2020.02.x版本.
2. 工具链升级到 gcc 7.2.0 版本.
3. 新增nor flash板级.
4. 新增nor flash多Die支持.
5. 优化ISP内核驱动.
    a. 新增MIPI摄像头.
    b. 优化ISP图像效果.
    c. 新增MIPI 4Lane ov4689 1080P@120fps.
6. 优化WIFI/eMMC/SD 性能.
7. 新增蓝牙功能．
8. 新增USB热插拔．
9. Fix其它已知问题.
10. 新增双摄demo.

---
 X2000_V12 kernel-4.4.93 Linux V2.0版本发布
 ---
 * 参考开发板:
    RD_X2000_HALLEY5_BASEBOARD_V2.0

TAGS:
    ingenic-linux-kernel4.4.93-x2000_v12-v2.0-20200514

* 实现功能:

uboot:
1. 支持nand烧录启动；
2. 支持SD卡烧录启动；

kernel:

1. 显示子系统功能：
    支持MIPI接口1080p/720p LCD显示；
2. 支持USB功能：
    a．Host端
        支持Ｕ盘、camera及hid鼠标功能；
    b．Device端
        支持camera、Ｕ盘、adb及hid鼠标功能；

3. 支持ISP双摄功能：
        支持DVPcamera　ov2735；

4. 支持SPI接口NAND；
5. 支持SDIO WiFi功能；
6. 支持watchdog功能；
7. 支持RTC、TCU及PWM功能；
8. 支持GMAC1单网口千兆以太网功能；
9. 支持Rotater功能；
10. 支持SPI、I2C功能；
11. 支持audio baic0 + icodec录放音通路正常；
12. 支持audio dmic录音功能正常；
13. 支持休眠唤醒功能；
14. 支持PDMA控制器功能；
15. 支持DTRNG随机数发生器功能；
16. 支持Nand的ota升级功能
